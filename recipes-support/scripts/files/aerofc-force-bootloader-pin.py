#!/usr/bin/python3
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html.
"""
Copyright (C) 2017  Intel Corporation. All rights reserved.
"""

import subprocess
import sys


def read_pin_value():
    print("Reading force bootloader pin...")
    try:
        output = subprocess.check_output(["/usr/bin/spi_xfer", "-b", "1", "-c", "1", "-d", "0001", "-w", "2"])
    except:
        print("Unable to read force bootloader pin value", file=sys.stderr)
        return

    lines = output.split('\n')
    words = lines[4].split()

    print("Force bootloader pin value =", words[2])


def set_pin_value(value):
    print("Writing force bootloader pin...")
    bits = '0181' if value else '0081'
    try:
        status, output = subprocess.getstatusoutput(["/usr/bin/spi_xfer", "-b", "1", "-c", "1", "-d", bits, "-w", "2"])
    except:
        print("Unable to set force bootloader pin value", file=sys.stderr)
        return
    
    print("Done!")

if len(sys.argv) < 2:
    read_pin_value()
    print("Help:", sys.argv[0], "<1 or 0> to set the force bootloader pin value")
else:
    value = sys.argv[1]
    set_pin_value(value != '0')
    read_pin_value()
