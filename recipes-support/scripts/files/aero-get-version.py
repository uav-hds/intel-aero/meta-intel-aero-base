#!/usr/bin/env python3

##############################################################################
# This script gets current versions of various software/firmware on Aero board.
# It reads current BIOS, OS, FPGA, and Airmap versions and prints on console.
#
# Author: Mandeep Bansal <mandeep.bansal@intel.com>
#      Pranav Tipnis <pranav.tipnis@intel.com>
#      Antoine Lima <antoine.lima@hds.utc.fr>
#
############################################################################

import subprocess
import os.path
import subprocess


def main():
    print("\n")
    BIOS_version()
    OS_version()
    AirMap_version()
    FPGA_version()
    AeroFC_version()
    print("\n")


def AeroFC_version():
    try:
        version = subprocess.check_output("/usr/sbin/aerofc-get-version.py")
    except:
        version = "unknown"

    print("AeroFC firmware version =", version)


def FPGA_version():
    try:
        # There is a bug in aero_sample and aero-rtf(now fixed) that was
        # causing a tranmission delay of 1 byte, that is why was
        # necessary to read it 2 times to get the right value and the
        # version was on the wrong byte.
        # So it will continue reading 2 times and comparing the values read
        # to decide what to show.
        output1 = subprocess.check_output(["/usr/bin/spi_xfer", "-b", "1", "-c", "1", "-d", "00", "-w", "2"])
        output2 = subprocess.check_output(["/usr/bin/spi_xfer", "-b", "1", "-c", "1", "-d", "00", "-w", "2"])

        lines = output1.split("\n")
        words = lines[4].split()
        v1 = words[2]

        lines = output2.split("\n")
        words = lines[4].split()
        v2 = words[2]

        version = v1 if v2 == v1 and v1 != "0x0" else words[1]
    except:
        version = "unknown"

    print("FPGA_VERSION =", version)


def AirMap_version():
    try:
        version = subprocess.check_output(["python" "/etc/airmap/AirMapSDK-Embedded/getver.py"])
    except:
        version = "unknown"

    print("AIRMAP_VERSION =", version)


def OS_version(rootdir="/"):
    version = None
    try:
        with open(os.path.join(rootdir, "etc", "os-release")) as f:
            for line in f:
                if line.startswith("PRETTY_NAME="):
                    version = line.split("PRETTY_NAME=")[1].strip('"').strip()
                    if version:
                        break
    except:
        pass

    if not version:
        version = "unknown"

    print("OS_VERSION =", version)


def BIOS_version():
    try:
        line = subprocess.check_output("dmidecode | grep Aero-", shell=True)
        version = line.split(":")[1].strip()
    except:
        version = "unknown"

    print("BIOS_VERSION =", version)


if __name__ == "__main__":
    main()
