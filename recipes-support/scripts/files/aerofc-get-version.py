#!/usr/bin/python3
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html


import sys
from time import time
from argparse import ArgumentParser
from pymavlink import mavutil


target_sys_id = 1
target_comp_id = mavutil.mavlink.MAV_COMP_ID_ALL
program_timeout = time() + 3
autopilot_version = 0

parser = ArgumentParser()
parser.add_argument("TCP_IP_PORT", default="127.0.0.1:5760")
args = parser.parse_args()
tcp_ip_port = args.TCP_IP_PORT

mav_con = mavutil.mavlink_connection("tcp:" + tcp_ip_port)
request_msg = mav_con.mav.command_long_encode(target_sys_id, target_comp_id,
    mavutil.mavlink.MAV_CMD_REQUEST_AUTOPILOT_CAPABILITIES,
    0, # confirmation
    1, # param1(request version)
    0, # param2
    0, # param3
    0, # param4
    0, # param5
    0, # param6
    0) # param7
version_request_timeout = 0

while True:
    msg = mav_con.recv_match(type="AUTOPILOT_VERSION", blocking=True, timeout=1)

    if msg is not None:
        autopilot_version = msg.flight_sw_version
        break

    if time() > program_timeout:
        break

    if time() > version_request_timeout:
        version_request_timeout = time() + 1
        mav_con.write(request_msg.pack(mav_con.mav))

if autopilot_version == 0:
    print("AeroFC firmware version = unknown")
    sys.exit()

majorVersion = (autopilot_version >> 24) & 0xFF
minorVersion = (autopilot_version >> 16) & 0xFF
patchVersion = (autopilot_version >> 8) & 0xFF

fw_version = f"{majorVersion}.{minorVersion}.{patchVersion}"
print("AeroFC firmware version =", fw_version)
